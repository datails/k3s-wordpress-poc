#!/bin/bash -x

RELEASE_NAME=vw-wordpress-release

# this does not work on a m1 mac, see: https://github.com/bitnami/charts/issues/7305
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install $RELEASE_NAME bitnami/wordpress

kubectl wait --namespace default \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/instance=$RELEASE_NAME
  --selector=app.kubernetes.io/component=controller \
  --timeout=120s

export SERVICE_IP=$(kubectl get svc --namespace default $RELEASE_NAME-wordpress --include "{{ range (index .status.loadBalancer.ingress 0) }}{{ . }}{{ end }}")
  echo "WordPress URL: http://$SERVICE_IP/"
  echo "WordPress Admin URL: http://$SERVICE_IP/admin"