# K3s + Docker + Wordpress

## Docker
To start WordPress in Docker, go to the `docker` directory and run:

```bash
docker-compose up -d
```

## K3s (kubernetes)
To Start WordPress with a HA MariaDB in K3s, install k3s using either k3d or plain k3s. Installation is done by running one of the scripts in the `k8s` directory.

### Single instance Wordpress
To rollout a single wordpress instance on a kubernetes cluster run:

```bash
kubectl apply -k ./k8s
```

For port-forwarding, see `README` in `k8s` directory.

### Multiple WordPress instance
To rollout a production ready WordPress instance, run:

```bash
chmod +x ./helm/prod-wordpress.sh && ./helm/prod-wordpress.sh
```

> NOTE: bitnami/wordpress is not support for Mac M1 yet. See [ticket](https://github.com/bitnami/bitnami-docker-wordpress/issues/316).