# Start WP locally using Docker

## Prerequisites
 - [docker](https://docs.docker.com/desktop/mac/install/)
 - docker compose

## Start

```bash
# start WP in detached mode
docker-compose up -d

# after some minutes, go in your browser to:
# localhost:8000 for the WP instance
# localhost:8080 for the phpMyAdmin
# login into phpMyAdmin with u: wordpress p: wordpress
```