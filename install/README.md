# Install K3s (lightweight Kubernetes)

## K3d
Use [k3d](https://k3d.io/v5.3.0/) to test the cluster locally. You can start a local cluster by running:

```bash
chmod +x ./k3d.sh
./k3d.sh
```

## K3s
Use [k3s](https://k3s.io/) on raspberry pi's. Create the cluster once with (x) control panes and add as much as nodes as needed.

```bash
chmod +x ./k3s.sh
./k3s.sh
```

## Debugging
Use a [VSCode plugin](https://marketplace.visualstudio.com/items?itemName=inercia.vscode-k3d) to debug the cluster.

## Monitoring
For monitoring use [k8slens](https://k8slens.dev/) (desktop app).