#!/bin/bash -x

CLUSTER_NAME=vw-cluster
KUBECONFIG_FILE=~/.kube/$CLUSTER_NAME

### 0. Install helm, kubectl (kubernetes-cli), and k3d with brew
brew install helm k3d kubectl

### 1. Create a cluster
# Optionally install with more agents `--agents 3`
k3d cluster create $CLUSTER_NAME --k3s-arg "--disable=traefik@server:0" --wait --verbose

### 2. Set up a kubeconfig so you can use kubectl in your current session
k3d kubeconfig get $CLUSTER_NAME > $KUBECONFIG_FILE
chmod 600 $KUBECONFIG_FILE
export KUBECONFIG=$KUBECONFIG_FILE
kubectl get nodes