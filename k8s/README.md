# K8s resources
This directory contains the k8s resources we need to run a single WordPress instance. 

## Install
If you don't already have a cluster, create a cluster from the `install` directory. If you have a cluster:

```bash
kubectl apply -k ./
```

To delete a resource:

```bash
kubectl delete -k ./
```

## Expose Wordpress outside cluster
After applying the resources, expose wordpress outside the cluster:

```bash
kubectl port-forward deployment/wordpress 8080:80 &
```

You can now hit (in a new terminal) wordpress at `localhost:8080`:
```bash
curl -v http://localhost:8080
```

> Note: kubectl port-forward does not return. To continue, you will need to open another terminal, or use an ampersand.

## Expose PhpMyAdmin outside cluster
After applying the resources, expose phpmyadmin outside the cluster:

```bash
kubectl port-forward deployment/phpmyadmin 8081:80 &
```

You can now hit (in a new terminal) phpmyadmin at `localhost:8080`:
```bash
curl -v http://localhost:8080
```

You can login with user: root, password: somewordpress.